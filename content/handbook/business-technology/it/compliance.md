---

title: "IT Compliance Team"
description: "This handbook page provides information about how the IT Compliance team works."
---







## Overview

This is a placeholder for the next iteration of the IT Compliance team's handbook page as part of our department's new information architecture.

In the interim, please visit the following handbook pages:

- [Security Best Practices for Team Members](/handbook/security)
- [IT Compliance](/handbook/business-technology/it-compliance)
- [Engineering Security Department](/handbook/security/) `#security` and `#security-department`
  - [Security Best Practices](/handbook/security/)
  - [Departmental Structure](/handbook/security/#departmental-structure)
  - [Security Assurance Sub-department](/handbook/security/security-assurance/)
    - [Field Security](/handbook/security/security-assurance/field-security/)
    - [Security Compliance](/handbook/security/security-assurance/security-compliance/)
    - [Security Governance](/handbook/security/security-assurance/governance/)
    - [Security Risk](/handbook/security/security-assurance/security-risk/)
  - [Product Security Sub-department](/handbook/security/product-security/)
    - [Application Security](/handbook/security/product-security/application-security/)
    - [Infrastructure Security](/handbook/security/product-security/infrastructure-security/)
    - [Security Research](/handbook/security/product-security/security-research/)
  - [Security Operations Sub-department](/handbook/security/security-operations)
    - [Security Incident Response Team "SIRT"](/handbook/security/security-operations/sirt)
    - [Trust and Safety](/handbook/security/security-operations/trustandsafety/)
    - [Security Logging](/handbook/security/security-operations/security-logging)
    - [Red Team](/handbook/security/security-operations/red-team)
    - [Security Research](/handbook/security/product-security/security-research/)
 [Team Member Enablement](/handbook/business-technology/end-user-services/) handbook page.
