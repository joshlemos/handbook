---

title: "GitLab swag program"
description: "This page outlines the swag program at GitLab."
---


## Welcome to the swag handbook

This page outlines everything you need to know about the swag program at GitLab. Whether you're ordering swag for yourself or others, follow the processes below to get started.

Don't see what you need on this page? Reach out to us via the `#swag` Slack channel and tag [the DRI](/handbook/marketing/brand-and-product-marketing/brand/merchandise-handling/#dris-for-the-swag-program) for your question.

## Swag shop

**June 7, 2024 update: Our swag shop and internal ordering process is offline for a few weeks while we transition to a new vendor. We'll be back online with some awesome new merch in early July, and our new swag ordering process will be outlined on this page. GitLab team members can check out [this issue](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-strategy/-/issues/401) for FAQs or urgent requests.**

***Note:*** Please do not work with other vendors to print your own swag. It's important that we all protect our brand integrity and avoid creating items that violate [brand standards](https://design.gitlab.com/).

### Suggesting new items or designs

Have feedback or suggestions for items you'd like to see in the swag shop? Please add them to [this issue](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-strategy/-/issues/401#have-other-questions-feedback-or-things-youd-like-to-see-in-the-new-shop-bulb).

## DRIs for the swag program

| Topic | DRI |
| ------ | ------ |
|    Swag program strategy, vendor relationships    |    Brand Strategy team: `@bbula`    |
|    Swag design    |    Brand Creative team: `@amittner`    |
|    New hire swag    |    Talent Brand team: `@drogozinski`    |
|   Anniversary swag     |    People Connect team: `@ameeks`    |

If you have other questions, please send a message to `@bbula` in the `#swag` Slack channel.
